<%@ include file="/libs/foundation/global.jsp" %>
<cq:setContentBundle />
<main id="main" class="home-page">
<div id="skrollr-body">
   <section class="section hero-section">
      <div class="container-fluid">
          <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure Hero section</div>
         </c:if>
         <cq:include path="herosection_par" resourceType="foundation/components/parsys" />
              </div></div>
      </div>
   </section>
   <!--Frontier whiskey Section -->
   <section id="frontierwhiskey" class="section frontier-whiskey">
      <div class="container">
           <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure Frontier Wiskey section</div>
         </c:if>
         <cq:include path="frontierwhiskey_par" resourceType="foundation/components/parsys" />
               </div></div>
      </div>
   </section>
   <!-- godivaliqueurs History Section-->
   <section id="the-story" class="section godivaliqueurs-history">
      <div class="container-fluid">
           <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure The History section</div>
         </c:if>
         <!-- Using Column control and rich text componets-->
         <cq:include path="thehistory_par" resourceType="foundation/components/parsys" />
               </div>
          </div>
      </div>
   </section>
   <!-- The Whiskey Section-->
   <section id="the-whiskey" data-track-id="analytics_product" class="section the-whisky">
      <div class="container">
           <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure The Whiskey section</div>
         </c:if>
         <cq:include path="thewhiskey_par" resourceType="foundation/components/parsys" />
               </div>
          </div>
      </div>
   </section>
   <!-- The Drinks Section-->
   <section id="whiskey-drinks" data-track-id="analytics_recipe" class="section related-product">
      <div class="container">
           <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure The Drinks section</div>
         </c:if>
         <cq:include path="thedrinks_par" resourceType="foundation/components/parsys" />
               </div>
          </div>
      </div>
   </section>
   <!-- Visit Us -->
   <section id="visit-us" class="section visit-us">
      <div class="container-fluid">
        <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure Visit Us section</div>
         </c:if>
         <cq:include path="visitus_par" resourceType="foundation/components/parsys" />
            </div></div>
      </div>
   </section>
   <!-- Contact Us -->
   <section id="contact-us" class="section contact-us">
      <div class="container">
           <div class="row">
			<div class="col-xs-12 full-grid">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
            && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
            <div style="color:red;">Please configure Contact Us section</div>
         </c:if>
         <cq:include path="contactus_par" resourceType="foundation/components/parsys" />
      </div>
          </div></div>
   </section>
</div>
   <!-- Contact Us ends here-->
</main>