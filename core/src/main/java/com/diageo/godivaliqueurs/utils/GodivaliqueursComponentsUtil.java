/*******************************************************************************
 * Copyright (c) 2015 Diageo.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Diageo.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.diageo.godivaliqueurs.utils;


import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diageo.platform.foundation.components.ComponentUtil;
import com.sapient.platform.iea.core.exception.SystemException;


/**
 * The Class DiageoContentDetailFetchUtil.
 */
public final class GodivaliqueursComponentsUtil{
	
 
}
